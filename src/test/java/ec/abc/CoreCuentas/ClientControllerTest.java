package ec.abc.CoreCuentas;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ec.abc.CoreCuentas.controller.AccountController;
import ec.abc.CoreCuentas.controller.ClientController;
import ec.abc.CoreCuentas.model.Account;
import ec.abc.CoreCuentas.model.Client;
import ec.abc.CoreCuentas.repository.ClientRepository;
import ec.abc.CoreCuentas.service.AccountService;
import ec.abc.CoreCuentas.service.ClientService;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientController.class)
public class ClientControllerTest {

    @MockBean
    private ClientService service;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createClient_whenPostMethod() throws Exception {

        Client c = new Client(10L);
        c.setAddress("ninguna");
        c.setAge(45);
        c.setGender('M');
        c.setIdentificationCard("5555555555");
        c.setName("Juan");
        c.setPassword("23541");
        c.setTelephone("32547");
        c.setUpdatedAt(new Date());
        c.setUsername("pablo");
        c.setState(true);

        given(service.save(c)).willReturn(c);

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(c);

        mockMvc.perform((RequestBuilder) ((ResultActions) MockMvcRequestBuilders
                .post("/api/client/client")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is(c.getName()))));
    }

}
