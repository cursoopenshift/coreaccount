package ec.abc.CoreCuentas.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import ec.abc.CoreCuentas.model.Transaction;

@Repository
public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long>,CustomTransactionRepository{
    
}
