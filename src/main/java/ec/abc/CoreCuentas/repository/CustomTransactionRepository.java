package ec.abc.CoreCuentas.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import ec.abc.CoreCuentas.model.util.Report;
import ec.abc.CoreCuentas.model.util.RequestTransaction;
import ec.abc.CoreCuentas.model.util.Response;

public interface CustomTransactionRepository {

    Optional<Response> executeTransaction(double limitDaily, RequestTransaction requestTransaction);

    List<Report> report(Date dateIni, Date dateEnd,String identification);
}
